package com.example.model;

public class Persona {

	private String dni;
	private String nombre;
	private int edad;
	private double sueldo;

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public double getSueldo() {
		return sueldo;
	}

	public void setSueldo(double sueldo) {
		this.sueldo = sueldo;
	}

	public Persona(String dni, String nombre, int edad, double sueldo) {
		super();
		this.dni = dni;
		this.nombre = nombre;
		this.edad = edad;
		this.sueldo = sueldo;
	}

	public Persona() {
		super();
	}

}
