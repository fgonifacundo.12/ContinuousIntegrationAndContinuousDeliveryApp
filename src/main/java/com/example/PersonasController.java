package com.example;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.Persona;

@RestController
@RequestMapping("/personas")
public class PersonasController {

	@GetMapping()
	public ResponseEntity<List<Persona>> allPersons() {
		List<Persona> personas = new ArrayList<>();

		personas.add(new Persona("45784578", " Jandri Justo", 18, 2000.00));
		personas.add(new Persona("55988854", " Jessica Ramos", 21, 1000.00));
		personas.add(new Persona("58484848", "Fabiola Cipriano", 25, 2500.00));

		return new ResponseEntity<List<Persona>>(personas, HttpStatus.OK);

	}
}
